# Changelog

## 0.3.1

- updated wsidicom

## 0.3.0

- Removed parameter validation (already part of wsi service)
- Removed special handling for out of image regions

## 0.2.0

- Updated to v3 models

## 0.1.0

- added wsidocom based plugin
